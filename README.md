# Gitlab CI Helpers

## Maven

```yaml
include:
  - project: "io_determan/gitlab-ci"
    ref: "master"
    file: "maven.yaml"
```